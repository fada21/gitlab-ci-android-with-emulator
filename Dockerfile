# based on https://hub.docker.com/r/jangrewe/gitlab-ci-android/builds/bbnfztangn5fzagdc5riaup/

FROM registry.gitlab.com/fada21/gitlab-ci-android-with-emulator/base:api27-x86-E.27.1.12-B.27.0.3-base
MAINTAINER Lukasz Czaplicki <lukasz.czaplicki@gmail.com>

ENV PATH "$PATH:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools"

ADD https://raw.githubusercontent.com/travis-ci/travis-cookbooks/94f697cf01ee1e5a274fca1d0719101969953098/community-cookbooks/android-sdk/files/default/android-wait-for-emulator android-wait-for-emulator
RUN chmod +x android-wait-for-emulator

RUN echo y | sdkmanager "system-images;android-25;google_apis;x86"
RUN echo "no" | avdmanager create avd -n test-x86 -k "system-images;android-25;google_apis;x86"

# # start emulator for tests
# ${ANDROID_HOME}/tools/emulator -avd test-x86 -skin 640x800 -noaudio -no-window -no-boot-anim -accel on -no-snapshot &
# ./android-wait-for-emulator

# # get installed packages list for image
# docker run -it registry.gitlab.com/fada21/gitlab-ci-android-with-emulator:api27-x86-E.27.1.12-B.27.0.3 --entrypoint "/bin/bash"
# /sdk/tools/bin/sdkmanager --list
