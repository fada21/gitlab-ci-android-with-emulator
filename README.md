# Android sdk and emulator for gitlab-ci setup

```
Installed packages:=====================] 100% Computing updates...             
  Path                                                                              | Version | Description                        | Location                                                                          
  -------                                                                           | ------- | -------                            | -------                                                                           
  add-ons;addon-google_apis-google-24                                               | 1       | Google APIs                        | add-ons/addon-google_apis-google-24/                                              
  build-tools;27.0.3                                                                | 27.0.3  | Android SDK Build-Tools 27.0.3     | build-tools/27.0.3/                                                               
  emulator                                                                          | 27.1.12 | Android Emulator                   | emulator/                                                                         
  extras;android;m2repository                                                       | 47.0.0  | Android Support Repository         | extras/android/m2repository/                                                      
  extras;google;google_play_services                                                | 48      | Google Play services               | extras/google/google_play_services/                                               
  extras;google;m2repository                                                        | 58      | Google Repository                  | extras/google/m2repository/                                                       
  extras;m2repository;com;android;support;constraint;constraint-layout-solver;1.0.2 | 1       | Solver for ConstraintLayout 1.0.2  | extras/m2repository/com/android/support/constraint/constraint-layout-solver/1.0.2/
  extras;m2repository;com;android;support;constraint;constraint-layout;1.0.2        | 1       | ConstraintLayout for Android 1.0.2 | extras/m2repository/com/android/support/constraint/constraint-layout/1.0.2/       
  patcher;v4                                                                        | 1       | SDK Patch Applier v4               | patcher/v4/                                                                       
  platform-tools                                                                    | 27.0.1  | Android SDK Platform-Tools         | platform-tools/                                                                   
  platforms;android-27                                                              | 1       | Android SDK Platform 27            | platforms/android-27/                                                             
  tools                                                                             | 26.1.1  | Android SDK Tools                  | tools/                                                                            


```

## previous setups

```
Installed packages:=====================] 100% Computing updates...             
  Path                                                                              | Version | Description                        | Location                                                                          
  -------                                                                           | ------- | -------                            | -------                                                                           
  add-ons;addon-google_apis-google-24                                               | 1       | Google APIs                        | add-ons/addon-google_apis-google-24/                                              
  build-tools;27.0.1                                                                | 27.0.1  | Android SDK Build-Tools 27.0.1     | build-tools/27.0.1/                                                               
  emulator                                                                          | 26.1.4  | Android Emulator                   | emulator/                                                                         
  extras;android;m2repository                                                       | 47.0.0  | Android Support Repository         | extras/android/m2repository/                                                      
  extras;google;google_play_services                                                | 46      | Google Play services               | extras/google/google_play_services/                                               
  extras;google;m2repository                                                        | 58      | Google Repository                  | extras/google/m2repository/                                                       
  extras;m2repository;com;android;support;constraint;constraint-layout-solver;1.0.2 | 1       | Solver for ConstraintLayout 1.0.2  | extras/m2repository/com/android/support/constraint/constraint-layout-solver/1.0.2/
  extras;m2repository;com;android;support;constraint;constraint-layout;1.0.2        | 1       | ConstraintLayout for Android 1.0.2 | extras/m2repository/com/android/support/constraint/constraint-layout/1.0.2/       
  patcher;v4                                                                        | 1       | SDK Patch Applier v4               | patcher/v4/                                                                       
  platform-tools                                                                    | 26.0.2  | Android SDK Platform-Tools         | platform-tools/                                                                   
  platforms;android-27                                                              | 1       | Android SDK Platform 27            | platforms/android-27/                                                             
  tools                                                                             | 26.1.1  | Android SDK Tools                  | tools/                                                                            


```

```
Installed packages:=====================] 100% Computing updates...             
  Path                                                                              | Version | Description                             | Location                                                                          
  -------                                                                           | ------- | -------                                 | -------                                                                           
  add-ons;addon-google_apis-google-24                                               | 1       | Google APIs                             | add-ons/addon-google_apis-google-24/                                              
  build-tools;26.0.2                                                                | 26.0.2  | Android SDK Build-Tools 26.0.2          | build-tools/26.0.2/                                                               
  emulator                                                                          | 26.1.4  | Android Emulator                        | emulator/                                                                         
  extras;android;m2repository                                                       | 47.0.0  | Android Support Repository              | extras/android/m2repository/                                                      
  extras;google;google_play_services                                                | 44      | Google Play services                    | extras/google/google_play_services/                                               
  extras;google;m2repository                                                        | 58      | Google Repository                       | extras/google/m2repository/                                                       
  extras;m2repository;com;android;support;constraint;constraint-layout-solver;1.0.2 | 1       | Solver for ConstraintLayout 1.0.2       | extras/m2repository/com/android/support/constraint/constraint-layout-solver/1.0.2/
  extras;m2repository;com;android;support;constraint;constraint-layout;1.0.2        | 1       | ConstraintLayout for Android 1.0.2      | extras/m2repository/com/android/support/constraint/constraint-layout/1.0.2/       
  patcher;v4                                                                        | 1       | SDK Patch Applier v4                    | patcher/v4/                                                                       
  platform-tools                                                                    | 26.0.1  | Android SDK Platform-Tools              | platform-tools/                                                                   
  platforms;android-26                                                              | 2       | Android SDK Platform 26                 | platforms/android-26/                                                             
  system-images;android-26;google_apis;x86                                          | 5       | Google APIs Intel x86 Atom System Image | system-images/android-26/google_apis/x86/                                         
  tools                                                                             | 26.1.1  | Android SDK Tools                       | tools/                                                                            
```
